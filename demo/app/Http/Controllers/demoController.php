<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpParser\Node\Expr\FuncCall;

class demoController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function form()
    {
        return view('form');
    }

    public function kirim(Request $request)
    {
        $name = $request->nama;
        $alamat = $request->address;
        return view('selamat', compact('name', 'alamat'));
    }

    public function welcome()
    {
        return view('index');
    }

    public function test()
    {
        return view('test');
    }

    public function table()
    {
        return view('table');
    }
}
