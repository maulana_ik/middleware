<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Uuid\UuidTrait;
class Role extends Model
{
	use UuidTrait;

	protected $guarded = [];   
     
      

    public function user(){
    	return $this->hasMany('App\User');
    }
}
