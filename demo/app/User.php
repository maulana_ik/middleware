<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use App\Uuid\UuidTrait;
use DB;
class User extends Authenticatable
{
    use Notifiable;
    use UuidTrait;

       

    protected $guarded = []; 
       
    protected function get_admin_id(){
        $role = \App\Role::where('name', 'admin')->first();
        return $role->id;
       }

    public static function boot(){
        parent::boot();
        static::creating(function($model){
            $model->role_id= $model->get_admin_id();
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    
    protected $fillable = [
        'id','name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

   public function otp(){

    return $this->hasOne('App\Otp_code', 'id_otp');
   }

   public function isAdmin(){
        if ($this->roleId == 1) {
            return true;
        }
        return false;
   }
}
