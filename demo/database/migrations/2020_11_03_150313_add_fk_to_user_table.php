<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
           $table->uuid('id_role');
           $table->uuid('id_otp');

           $table->foreign('id_role')->references('id')->on('roles')->onDelete('cascade');
           $table->foreign('id_otp')->references('id')->on('otp_code')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['id_otp']);
            $table->dropForeign(['id_role']);
            $table->dropColumn(['id_otp']);
            $table->dropColumn(['id_role']);
        });
    }
}
